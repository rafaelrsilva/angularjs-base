angular.module('app', ['ngRoute', 'ngAnimate'])

.run(function($rootScope){
	$rootScope.page = {
		title        : '',
		description  : '',
		site_name    : '',
		author       : '',
		url          : '',
		image        : '',
		image_type   : '',
		image_width  : '',
		image_height : '',
	}
})

.config(function($locationProvider, $routeProvider, $sceProvider){
	$locationProvider.hashPrefix('').html5Mode(true);

	$sceProvider.enabled(false);

	$routeProvider.when('/', {
		templateUrl: 'views/home.view.html',
		controller: 'homeController',
		reloadOnSearch: false
	});

	$routeProvider.otherwise('/');
});