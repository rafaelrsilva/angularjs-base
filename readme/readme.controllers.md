# AngularJS: Controllers

## Arquivos Necessários

Dentro da pasta **application/controllers**, deve ser criado o seguinte arquivo:

- **nome-do-controller**.controller.js
```js
angular.module('app').controller('nomeDoController', function($scope){
	//...
});
```

## Utilização 

Informar o nome do controller em **controller** ao definir uma rota:

```js
angular.module('app', []).config(function($routeProvider){
	//...

	$routeProvider.when('/rota/desejada/para/utilizar/o/controller', {
		//...
		controller: 'nomeDoController',
		//...
	});
	
	//...
});
```

## Mais informações

Para mais informações sobre controllers, consultar a [documentação do AngularJS](https://docs.angularjs.org/guide/controller).