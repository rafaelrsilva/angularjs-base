# AngularJS: Filtros

## Arquivos Necessários

Dentro da pasta **application/filters**, deve ser criado o seguinte arquivo:

- **nome-do-filtro**.directive.js
```js
angular.module('app').filter('nomeDoFiltro', function(){
    return function(input) {
        //...
    };
});
```

## Mais informações

Para mais informações sobre filtros, consultar a [documentação do AngularJS](https://docs.angularjs.org/guide/filter).