# AngularJS: Constantes

## Adição de Novas Constantes

No arquivo **application/constants/constants.js**, seguir o formato informado:

```js
angular.module('app').constant('AppConstants', (function(){
	var constants = {};
	//...
	Object.defineProperties(constants, {
		//...
 		nomeDaConstante: { value: 'valorDaConstante' },
 		//...
	});
	
	return constants;
}()));
```


## Exemplo de Utilização

- No controller:
```js
angular.module('app').controller('controllerQualquer', function($scope, AppConstants){
	//...
	$scope.atributoQualquer = AppConstants.nomeDaConstante;
	//...
});
```

**Pode ser utilizado em qualquer outro lugar onde se possa fazer injeção de dependências.**