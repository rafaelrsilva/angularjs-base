# AngularJS: HTTP Interceptors

## Arquivos Necessários

Dentro da pasta **application/interceptors**, deve ser criado o seguinte arquivo:

- **nome-do-interceptor**.interceptor.js
```js
angular.module('app').factory('NomeDoInterceptor', function($q, /*...*/) {
	return {
		request: function(config) {
			//...
			return config;
		},

		requestError: function(rejection) {
			//...

			if (canRecover(rejection)) {
				return responseOrNewPromise
			}

			return $q.reject(rejection);
		},

	    response: function(response) {
			//..
			return response;
	    },

		responseError: function(rejection) {
			//...

			if (canRecover(rejection)) {
				return responseOrNewPromise
			}

			return $q.reject(rejection);
		}
	};
});
```

## Registrar o Interceptor

- No arquivo **application/application.js**:
```js
angular.module('app', ['ngRoute', 'ngAnimate', */...*/])
//...
.config(function($routeProvider, $httpProvider, /*...*/){
	//...
		$httpProvider.interceptors.push('NomeDoInterceptor');
	//...
});
//...
```


## Mais informações

Para mais informações sobre interceptors, consultar a [documentação do AngularJS](https://docs.angularjs.org/api/ng/service/$http#interceptors).