# AngularJS: Diretivas

## Arquivos Necessários

Dentro da pasta **application/directives**, criar uma pasta para a diretiva, seguindo o padrão **nome-da-diretiva**. Dentro dessa pasta, deve ser criado dois arquivos:

- **nome-da-diretiva**.directive.html
```html
<script type="text/ng-template" id="directives/{nome-da-diretiva}.directive.html">
	<!--- ... -->
</script>
```

	**Para evitar conflitos com html de views, é recomendável seguir o formato do nome do arquivo, bem como valor do atributo id**

- **nome-da-diretiva**.directive.js
```js
angular.module('app').directive("nomeDaDiretiva", function() {
    return {
    	restrict: 'EA',
        templateUrl: 'directives/{nome-da-diretiva}.directive.html'
    };
});
```



## Utilização 

- Como elemento:
```html
<nome-da-diretiva></nome-da-diretiva>
```

- Como atributo HTML:
```html
<div nome-da-diretiva></div>
```

## Mais informações

Para mais informações sobre diretivas, consultar a [documentação do AngularJS](https://docs.angularjs.org/guide/directive).