# AngularJS: Views

## Arquivos Necessários

Dentro da pasta **application/views**, deve ser criado o seguinte arquivo:

- **nome-da-view**.view.html
```html
<script type="text/ng-template" id="views/{nome-da-view}.view.html">
	<!--- ... -->
</script>
```

**Para evitar conflitos com html de diretivas, é recomendável seguir o formato do nome do arquivo, bem como valor do atributo id**

## Utilização 

Informar o valor do atributo **id** da view em **templateUrl** ao informar a rota para a view criada

```js
angular.module('app', [/*...*/]).config(function(/*...*/, $routeProvider, /*...*/){
	//...

	$routeProvider.when('/rota/desejada/para/carregar/a/view', {
		templateUrl: 'views/{nome-da-view}.view.html',
		//...
	});
	
	//...
});
```