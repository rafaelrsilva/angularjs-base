# AngularJS: Services

## Arquivos Necessários

Dentro da pasta **application/services**, deve ser criado o seguinte arquivo:

- **nome-do-service**.service.js
```js
angular.module('app').factory('NomeDoService', function(){
	//Funções e variáves privadas
	
	return {
		//Métodos e atributos públicos
	};
});
```

## Exemplo de Utilização

- No controller:
```js
angular.module('app').controller('controllerQualquer', function($scope, /*...*/, NomeDoService, /*...*/){
	//...
	NomeDoService.metodoPublico();
	//...
	$scope.atributoQualquer = NomeDoService.atributoPublico;
	//...
});
```

**Pode ser utilizado em qualquer outro lugar onde se possa fazer injeção de dependência.**

## Mais informações

Para mais informações sobre services, consultar a [documentação do AngularJS](https://docs.angularjs.org/guide/services).