var gulp     	 = require('gulp');
var gutil 		 = require('gulp-util');
var uglify   	 = require("gulp-uglify");
var concat   	 = require("gulp-concat");
var cleanCSS 	 = require('gulp-clean-css');
var minifyHTML   = require('gulp-minify-html');
var minifyInline = require('gulp-minify-inline');
var clean 		 = require('gulp-clean');
var rename 		 = require('gulp-rename');
var watch 		 = require('gulp-watch');

var files = {
	vendors: {
		js: [
			'application/assets/vendors/jquery/jquery-3.2.1.min.js',
			'application/assets/vendors/jquery/jquery-migrate-1.4.1.min.js',
			'application/assets/vendors/bootstrap/js/bootstrap.min.js',
			'application/assets/vendors/swiper/js/swiper.min.js',
			'application/assets/vendors/angular/angular.min.js',
			'application/assets/vendors/angular/angular-route.min.js',
			'application/assets/vendors/angular/angular-animate.min.js',
			'application/assets/vendors/**/*.js',
		],
		css: [
			'application/assets/vendors/bootstrap/css/bootstrap.min.css',
			'application/assets/vendors/bootstrap/css/bootstrap-theme.min.css',
			'application/assets/vendors/swiper/css/swiper.min.css',
			'application/assets/vendors/**/*.css',
		],
	},
	app: {
		js: [
			'application/application.js',
			'application/constants/constants.js',
			'application/interceptors/**/*.js',
			'application/services/**/*.js',
			'application/filters/**/*.js',
			'application/directives/**/*.js',
			'application/controllers/**/*.js',
		],
		css: [
			'application/assets/css/main.css',
		],
		html: [
			'application/includes/header.html',
			'application/includes/view.html',
			'application/directives/**/*.html',
			'application/views/**/*.html',
			'application/includes/footer.html',
		],
	}
};

var packJS = function(input, output){
	return gulp.src(input)
    	.pipe(concat(output))
    	.pipe(uglify({ mangle: false }))
    	.on('error', function(error){ gutil.log(error.toString()); this.emit('end'); })
    	.pipe(gulp.dest('application/assets/js'));
};

var packCSS = function(input, output){
	return gulp.src(input)
		.pipe(concat(output))
		.pipe(cleanCSS({level: {1: {specialComments: 0}}}))
		.pipe(gulp.dest('application/assets/css'));
};

const tasksGroups = {
	pack: {
		'pack-vendors-js': function(){
			return packJS(files.vendors.js, 'vendors.min.js');
		},
		'pack-vendors-css': function(){	
			return packCSS(files.vendors.css, 'vendors.min.css');
		},
		'pack-app-js': function(){
			return packJS(files.app.js, 'app.min.js');
		},
		'pack-app-css': function(){	
			return packCSS(files.app.css, 'app.min.css');
		},
		'pack-app-html': function(){
			return gulp.src(files.app.html).pipe(concat('index.html')).pipe(minifyHTML({ empty: true, spare: true, quotes: true })).pipe(minifyInline()).pipe(gulp.dest(''));
		}
	},
	distMake: {
		'dist-make-1': function(){
			return gulp.src('index.html').pipe(gulp.dest('dist'));
		},
		'dist-make-2': function(){
			return gulp.src('dist.htaccess').pipe(rename('.htaccess')).pipe(gulp.dest('dist'));
		},
		'dist-make-3': function(){
			return gulp.src('application/assets/js/app.min.js').pipe(gulp.dest('dist/application/assets/js'));
		},
		'dist-make-4': function(){
			return gulp.src('application/assets/js/vendors.min.js').pipe(gulp.dest('dist/application/assets/js'));
		},
		'dist-make-5': function(){
			return gulp.src('application/assets/css/app.min.css').pipe(gulp.dest('dist/application/assets/css'));
		},
		'dist-make-6': function(){
			return gulp.src('application/assets/css/vendors.min.css').pipe(gulp.dest('dist/application/assets/css'));
		},
		'dist-make-7': function(){
			return gulp.src("application/assets/fonts/**/*").pipe(gulp.dest('dist/application/assets/fonts'));
		},
		'dist-make-8': function(){
			return gulp.src('application/assets/img/**/*').pipe(gulp.dest('dist/application/assets/img'));
		}
	}
};

for(var tg in tasksGroups){
	for(var t in tasksGroups[tg]){
		gulp.task(t, tasksGroups[tg][t]);
	}
}

gulp.task('dist-clean', function(){
	return gulp.src('dist').pipe(clean());
});

gulp.task('dist', ['dist-clean'].concat(Object.keys(tasksGroups.pack)), function(){
	gulp.start(Object.keys(tasksGroups.distMake), function(){
		var fs = require('fs');
		var baseurl = '/';

		if(gutil.env.baseurl){
			baseurl = '/' + gutil.env.baseurl.replace(new RegExp('\/+', 'g'), '/').replace(new RegExp('^\/+|\/+$', 'g'), '') + '/';
		}

		var indexHTML = fs.readFileSync('dist/index.html', 'utf8');

		if(!gutil.env.notmerge){
			var vendorsCSS = '<style>' + fs.readFileSync('dist/application/assets/css/vendors.min.css', 'utf8') + '</style>';
			var vendorsJS  = '<script>' + fs.readFileSync('dist/application/assets/js/vendors.min.js', 'utf8') + '</script>';
			var appCSS     = '<style>' + fs.readFileSync('dist/application/assets/css/app.min.css', 'utf8') + '</style>';
			var appJS      = '<script>' + fs.readFileSync('dist/application/assets/js/app.min.js', 'utf8') + '</script>';
			
			var css = vendorsCSS + appCSS;
			var js = vendorsJS + appJS; 

			var cssStartTag = '<meta name="gulp-assets-css-start">';
			var cssEndTag = '<meta name="gulp-assets-css-end">';
			var regexCSS = new RegExp('(?<=' + cssStartTag + ').*?(?=' + cssEndTag + ')', 'g');

			indexHTML = indexHTML.replace(regexCSS, '');
			indexHTML = indexHTML.split((cssStartTag + cssEndTag));
			indexHTML = indexHTML[0] + css + indexHTML[1];	

			var jsStartTag = '<meta name="gulp-assets-js-start">';
			var jsEndTag = '<meta name="gulp-assets-js-end">';
			var regexJS = new RegExp('(?<=' + jsStartTag + ').*?(?=' + jsEndTag + ')', 'g');

			indexHTML = indexHTML.replace(regexJS, '');
			indexHTML = indexHTML.split((jsStartTag + jsEndTag));
			indexHTML = indexHTML[0] + js + indexHTML[1];	

			gulp.src(['dist/application/assets/css', 'dist/application/assets/js']).pipe(clean());
		}

		var baseurlStartTag = '<meta name="gulp-angular-baseurl-start">';
		var baseurlEndTag = '<meta name="gulp-angular-baseurl-end">';
		var regexBase = new RegExp('(?<=' + baseurlStartTag + ').*?(?=' + baseurlEndTag + ')', 'g');

		indexHTML = indexHTML.replace(regexBase, '');
		indexHTML = indexHTML.replace((baseurlStartTag + baseurlEndTag), '<base href="' + baseurl + '">');

		fs.writeFileSync('dist/index.html', indexHTML);
		fs.writeFileSync('dist/.htaccess', fs.readFileSync('dist/.htaccess', 'utf8').replace('/index.html', baseurl + 'index.html'));

		gutil.log('A versão de distribuição foi gerada com sucesso.');
	});	
});

gulp.task('default', Object.keys(tasksGroups.pack), function(){
	watch(files.vendors.js, function(){ gulp.start('pack-vendors-js'); });
	watch(files.vendors.css, function(){ gulp.start('pack-vendors-css'); });
	watch(files.app.js, function(){ gulp.start('pack-app-js'); });
 	watch(files.app.css, function(){ gulp.start('pack-app-css'); });
	watch(files.app.html, function(){ gulp.start('pack-app-html'); });

	setTimeout(function(){
		gutil.log('Ambiente pronto para desenvolvimento.');
	}, 50);
});