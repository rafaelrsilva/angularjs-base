# Descrição

Este repositório é uma base para iniciar o desenvolvimento utilizando AngularJS. Esta base foi estruturada e desenvolvida prevendo o uso dos seguintes recursos do AngularJS:

- Controllers
- Diretivas (Directives)
- Filtros (Filters)
- Interceptadores HTTP (Interceptors)
- Serviços (Services)
- Views

Este arquivo contém o passo a passo para:

- Configuração do Projeto
- Desenvolvimento do Projeto
- Publicação do Projeto em Produção

---

# Configuração do Projeto

Para iniciar o desenvolvimento, é necessário seguir algumas etapas de configuração:

- Configuração do htaccess
- Configuração do AngularJS
- Configuração do Gulp

## Configuração do htaccess

É necessário alterar o caminho para o arquivo **index.html** no **htaccess**. 

Alterar:
	
	RewriteRule ^ /angularjs-base/index.html  

Para:

	RewriteRule ^ /caminho/para/pasta/do/projeto/index.html  

**OBS.:** Deixar como `RewriteRule ^ /index.html` caso o projeto vá rodar na raíz do servidor.

## Configuração do Angular

Alterar o valor do atributo **href** da tag **base**, no arquivo **application/includes/header.html**

De:

```html
<base href="/angularjs-base/">
```
Para:

```html
<base href="/caminho/para/pasta/do/projeto/">
```

**OBS.:** Deixar o valor como `/` caso o projeto vá rodar na raíz do servidor.

## Configuração do Gulp

O gulp é utilizado para fazer a minificação e empacotamento dos arquivos **js**, **css** e **html**, bem como gerar a **versão de distribuição** do projeto. Para iniciar o desenvolvimento do projeto, é **obrigatório** que ele esteja instalado e funcionando. Para isso é só seguir os seguintes passos:

1. Para utilizar o gulp, é necessário ter o NodeJS instalado. Ele pode ser baixado [aqui](https://nodejs.org/en/download/current).
2. Após o node instalado, executar o seguinte comando no terminal:

		sudo npm install --global gulp-cli

	**OBS.**: Não é necessário o comando `sudo` se estiver utilizando sistema operacional **linux**.

3. Instalar as dependências do gulp. Para isso vá para a **raíz do projeto**, via **terminal** e execute o seguinte comando:

		sudo npm install

	**OBS.**: Não é necessário o comando `sudo` se estiver utilizando sistema operacional **linux**.

4. Testar o funcionamento do gulp utilizando o seguinte comando:

		gulp

5. Se tudo rodou como esperado, você verá uma saída parecida com a saída abaixo

		[HH:MM:SS] Using gulpfile ~/caminho/para/o/arquivo/gulpfile.js
		[HH:MM:SS] Starting 'pack-vendors-js'...
		[HH:MM:SS] Starting 'pack-vendors-css'...
		[HH:MM:SS] Starting 'pack-app-js'...
		[HH:MM:SS] Starting 'pack-app-css'...
		[HH:MM:SS] Starting 'pack-app-html'...
		[HH:MM:SS] Finished 'pack-app-css' after 480 ms
		[HH:MM:SS] Finished 'pack-vendors-css' after 589 ms
		[HH:MM:SS] Finished 'pack-app-js' after 587 ms
		[HH:MM:SS] Finished 'pack-vendors-js' after 8.37 s
		[HH:MM:SS] Finished 'pack-app-html' after 8.35 s
		[HH:MM:SS] Starting 'default'...
		[HH:MM:SS] Finished 'default' after 18 ms
		[HH:MM:SS] Ambiente pronto para desenvolvimento.

**OBS**: O gulp deve ser configurado sempre que iniciar/continuar o desenvolvimento em um nova máquina.

---

# Desenvolvimento do Projeto

Se todo o processo de configuração descrito acima foi executado sem nenhum erro, ao acessar o projeto no navegador será exibido a mensagem abaixo:

	Base AngularJS
	Se você está vendo esta página, significa que o projeto foi carregado sem problemas.

A partir daqui já se pode iniciar/continuar o desenvolvimento do projeto

## Estrutura do Projeto

		|- application
		|	|- assets
		|	|	|- css
		|	|	|	|- main.css
		|	|	|	|- **vendors.min.css**
		|	|	|	|- **app.min.css**
		|	|	|	
		|	|	|- fonts
		|	|	|- img
		|	|	|- js
		|	|	|	|- **vendors.min.js**
		|	|	|	|- **app.min.js**
		|	|	|
		|	|	|- vendors
		|	|
		|	|- constants
		|	|	|-constants.js
		|	|
		|	|- controllers
		|	|- directives
		|	|- filters
		|	|- includes
		|	|	|- footer.html
		|	|	|- header.html
		|	|	|- view.html
		|	|
		|	|- interceptors
		|	|- services
		|	|- views
		|	|- application.js
		|
		|- .htaccess
		|- dist.htaccess
		|- gulpfile.js
		|- **index.html**

**\*\*Arquivos empacotados, gerados pelo Gulp\*\***
 
## Descrição dos Arquivos Empacotados

Esta base está programada para carregar **apenas os arquivos gerados pelo gulp**, afim de reduzir o número de requisições no navegador. Abaixo está a descrição de cada um, bem como a composição de cada:

- **index.html**: Arquivo minificado contendo a concatenação dos seguintes arquivos:

		application/includes/header.html
		application/includes/view.html
		application/views/**/*.html
		application/directives/**/*.html
		application/includes/footer.html

- **vendors.min.js**: Arquivo minificado contendo a concatenação de todos os arquivos JS de terceiros:
	
		application/assets/vendors/**/*.js

- **vendors.min.css**: Arquivo minificado contendo a concatenação de todos os arquivos CSS de terceiros:

		application/assets/vendors/**/*.css

- **app.min.js**: Arquivo minificado contendo a concatenação de todos os arquivos JS do projeto:

		application/application.js
		application/constants/constants.js
		application/interceptors/**/*.js
		application/services/**/*.js
		application/filters/**/*.js
		application/directives/**/*.js
		application/controllers/**/*.js

- **app.min.css**: Arquivo minificado contendo o css do projeto:

		application/assets/css/main.css

## Utilização do Gulp

Esta base já está programada para executar as seguintes tarefas:

- **pack-vendors-js**: Gera o arquivo **vendors.min.js**.
		
		gulp pack-vendors-js

- **pack-vendors-css**: Gera o arquivo **vendors.min.css**.

		gulp pack-vendors-css

- **pack-app-js**: Gera o aquivo **app.min.js**.

		gulp pack-app-js

- **pack-app-css**: Gera o arquivo **app.min.css**.

		gulp pack-app-css

- **pack-app-html**: Gera o arquivo **index.html**.

		gulp pack-app-html

- **default**: Comando default do gulp.

	Esta tarefa executa todas as tarefas de empacotamento acima, além de manter o gulp em execução monitorando a alteração, inclusão e remoção de arquivos. **O gulp não monitora remoção de uma pasta inteira.**

		gulp

- **dist**: Cria a versão de distribuição.
	
	Será criado uma pasta **dist**, contendo todos os arquivos necessários para publicação em produção. Se houver uma versão anterior gerada, a mesma é removida antes de gerar a nova versão.

		gulp dist [--baseurl caminho/pasta/aplicacao] [--notmerge]

	- **--baseurl**: altera a URL base da aplicação. **Default**: `/` (aplicação rodando na raíz do servidor)
	- **--notmerge**: evita que toda a aplicação (html + css + js) seja empacotada em um único arquivo (index.html)


- **dist-clean**: Remove a ultima versão de distribuição gerada;
	
	Remove a pasta **dist** e todo o seu conteúdo.

		gulp dist-clean

Para a implementaçõa de novas tarefas, utilizar o arquivo **gulpfile.js**. Caso necessite de mais informações, consulte o [site do gulp](https://gulpjs.com/).

## Desenvolvimento

Duranto o desenvolvimento, é recomendável que você mantenha uma janela do terminal sempre aberta e executando a tarefa **default** do gulp:

		gulp

Isso garante que todas as alterações feitas estão sendo refletidas no navegador.


Abaixo segue links de ajuda para criação dos recursos **no formato exato esperado por esta base**:

- [Criação de Constantes](https://bitbucket.org/rafaelrsilva/angularjs-base/src/master/readme/readme.constants.md)
- [Criação de Controllers](https://bitbucket.org/rafaelrsilva/angularjs-base/src/master/readme/readme.controllers.md).
- [Criação de Diretivas](https://bitbucket.org/rafaelrsilva/angularjs-base/src/master/readme/readme.directives.md).
- [Criação de Filtros](https://bitbucket.org/rafaelrsilva/angularjs-base/src/master/readme/readme.filters.md).
- [Criação de Interceptadores](https://bitbucket.org/rafaelrsilva/angularjs-base/src/master/readme/readme.interceptors.md).
- [Criação de Services](https://bitbucket.org/rafaelrsilva/angularjs-base/src/master/readme/readme.services.md).
- [Criação de Views](https://bitbucket.org/rafaelrsilva/angularjs-base/src/master/readme/readme.views.md).

# Publicação do Projeto em Produção

No terminal, na raíz do projeto, executar o comando `gulp dist`. **Se a aplicação não estiver na raíz do servidor**, será necessário passar o parâmetro `--baseurl`, informando o caminho para a pasta da aplicação no servidor.

Será criado uma pasta chamada **dist**, contendo todos os arquivos necessários para publicação.